from django.shortcuts import render, HttpResponse, redirect
from datetime import datetime
from hrstkopedie.settings import *
from django.core.files.storage import FileSystemStorage
import json
import operator
from . import models

def newculture(request):
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")
	image_user = models.run_query("select image from users where email='" + useremail_cookie +  "'")
	user_name = models.run_query("select name from users where email='" + useremail_cookie +  "'")
	user_id =models.run_query("select user_id from users where email='" + useremail_cookie +  "'") 
	dict_items = models.run_query_get_all("select name from book;")
	dict_items.sort()
	list_of_passwords=[]
	for dict_item in dict_items:
		password_ids = models.run_query("select book_id from book where name='" + dict_item +  "'")
		list_of_passwords.append([dict_item, password_ids])


	if request.method == "POST":
		try:
			culture_type = request.POST['type_culturexx']
		except:
			culture_type = None
		try:
			culture_name = (request.POST["culture_name"])
		except:
			culture_name =None
		try:
			culture_style = (request.POST["culture_style"])
		
		except:
			culture_style = None
		try:
			culture_description = (request.POST["culture_description"])
		except: 
			culture_description = None
		if culture_name == None or culture_style == None or culture_type ==None:
			status_message=""
			error_msg = "Je třeba vyplnit alespoň název a žánr."
			response = render(request, "main/new_culttemplate.html", dict(home_url=home_url,user_id=user_id, error_msg=error_msg, status_message=status_message,muj_list=list_of_passwords, user_name=user_name,image_user=image_user ))
			return response	

#		try:
		myfile = request.FILES['myfile']
		fs = FileSystemStorage()
		new_img_id = models.get_new_image_id()
		new_image_name = new_img_id + ".png"
		filename = fs.save(new_image_name, myfile)
		uploaded_file_url = fs.url(filename)
		uploaded_img = True
#		except:
#			uploaded_img = False


		new_culture_id = models.get_new_culture_id()


		if uploaded_img == True:
			models.update_query("insert into culture (culture_id,name,type,style,description,image,author,create_date) values (" + new_culture_id +", '" + culture_name + "', '" + culture_type + "', '" +  culture_style + "', '"+ culture_description + "' , '" + new_image_name + "' , '" + str(user_id) + "' , '" + str(datetime.now()) +"') ")
		
		if uploaded_img == False:
			models.update_query("insert into culture (culture_id,name,type,style,description,author,create_date) values (" + new_culture_id +", '" + culture_name + "', '" + culture_type + "', '" +  culture_style + "', '"+ culture_description + "' , '" + str(user_id) + "' , '" + str(datetime.now()) +"') ")
		response=culture(request, True, new_culture_id)
		return response

	else:
		status_message=""
		response = render(request, "main/new_culttemplate.html", dict(home_url=home_url, user_id=user_id, status_message=status_message,muj_list=list_of_passwords, user_name=user_name,image_user=image_user ))
		return response



def like(request, like_id):
	# check if user is logged
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")


	if password_cookie_hash != None and useremail_cookie != None:
		db_password = models.run_query("select password from users where email='" + useremail_cookie +  "'")
		if password_cookie_hash == db_password:
			print("logged")
			logged_user = True
		else:
			logged_user=False
			print("unloged - bad password")
	else:
		logged_user=False
		print("unloged")

	if logged_user == False:
		return redirect('/login')

	user_id =models.run_query("select user_id from users where email='" + useremail_cookie +  "'") 
	likers_list_raw = models.run_query("select likes from culture where culture_id='" + str(like_id) +  "'")
	if likers_list_raw != None:
		likes_list = json.loads(likers_list_raw)
	else:
		likes_list = []

	if user_id not in likes_list:
		likes_list.append(user_id)
		models.update_query("update culture set likes='" + str(likes_list) +  "' where culture_id=" +  str(like_id) + ";")

	response=culture(request, True, like_id)

	return response




def logout(request):
	error_msg=""
	response = render(request, "login/logout_template.html", dict(home_url=home_url,error_msg=error_msg))

	response.delete_cookie("hrstka_password")
	response.delete_cookie("hrstka_email")
	return (response)

def culture(request, liked=False, liked_val=0):
	# check if user is logged
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")
	if password_cookie_hash != None and useremail_cookie != None:
		db_password = models.run_query("select password from users where email='" + useremail_cookie +  "'")
		if password_cookie_hash == db_password:
			print("logged")
			logged_user = True
		else:
			logged_user=False
			print("unloged - bad password")
	else:
		logged_user=False
		print("unloged")

	if logged_user == False:
		return redirect('/login')




	dict_items = models.run_query_get_all("select name from book;")
	dict_items.sort()
	list_of_passwords=[]
	for dict_item in dict_items:
		password_id = models.run_query("select book_id from book where name='" + dict_item +  "'")
		list_of_passwords.append([dict_item, password_id])
	user_name = models.run_query("select name from users where email='" + useremail_cookie +  "'")
	image_user = models.run_query("select image from users where email='" + useremail_cookie +  "'")
	user_id =models.run_query("select user_id from users where email='" + useremail_cookie +  "'") 
	if logged_user == False:
		response.delete_cookie("hrstka_password")
		response.delete_cookie("hrstka_email")



	# FILMY 
	film_id_list = models.run_query_get_all("select culture_id from culture where type='film';")
	all_films_complet_list = []
	# one film in list:        0        1          2           3               4         5            6           7
	#		one_film_list= [film_id,film_name, film_style,film_description,film_image,autor_name,autor_image, likes_list  ]
	for film_id in film_id_list:
		film_name = models.run_query("select name from culture where culture_id='" + str(film_id) + "';")
		film_style= models.run_query("select style from culture where culture_id='" + str(film_id) + "';")
		film_description=models.run_query("select description from culture where culture_id='" + str(film_id) + "';")
		autor_id = models.run_query("select author from culture where culture_id='" + str(film_id) + "';")
		autor_name= models.run_query("select name from users where user_id='" + str(autor_id) + "';") 
		autor_image=models.run_query("select image from users where user_id='" + str(autor_id) + "';") 
		film_image = models.run_query("select image from culture where culture_id='" + str(film_id) + "';")
		if film_image == None:
			film_image="no_image.png"
		likes = models.run_query("select likes from culture where culture_id='" + str(film_id) + "';")
		if likes != None:
			likes = json.loads(likes)
			likes_list = []
			for like_id in likes:
				liker_name= models.run_query("select name from users where user_id='" + str(like_id) + "';") 
				liker_image=models.run_query("select image from users where user_id='" + str(like_id) + "';") 	
				one_liker_list=[liker_name, liker_image]
				likes_list.append(one_liker_list)
		else:
			likes_list = []
		one_film_list= [film_id,film_name, film_style,film_description,film_image,autor_name,autor_image, likes_list  ]
		all_films_complet_list.append(one_film_list)
	all_films_complet_list = sorted(all_films_complet_list, key=operator.itemgetter(1))

	# HUDBA
	music_id_list = models.run_query_get_all("select culture_id from culture where type='music';")
	all_musics_complet_list = []
	# one music in list:        0        1          2           3               4         5            6           7
	#		one_music_list= [music_id,music_name, music_style,music_description,music_image,autor_name,autor_image, likes_list  ]
	for music_id in music_id_list:
		music_name = models.run_query("select name from culture where culture_id='" + str(music_id) + "';")
		music_style= models.run_query("select style from culture where culture_id='" + str(music_id) + "';")
		music_description=models.run_query("select description from culture where culture_id='" + str(music_id) + "';")
		autor_id = models.run_query("select author from culture where culture_id='" + str(music_id) + "';")
		autor_name= models.run_query("select name from users where user_id='" + str(autor_id) + "';") 
		autor_image=models.run_query("select image from users where user_id='" + str(autor_id) + "';") 
		music_image = models.run_query("select image from culture where culture_id='" + str(music_id) + "';")
		if music_image == None:
			music_image="no_image.png"
		likes = models.run_query("select likes from culture where culture_id='" + str(music_id) + "';")
		if likes != None:
			likes = json.loads(likes)
			likes_list = []
			for like_id in likes:
				liker_name= models.run_query("select name from users where user_id='" + str(like_id) + "';") 
				liker_image=models.run_query("select image from users where user_id='" + str(like_id) + "';") 	
				one_liker_list=[liker_name, liker_image]
				likes_list.append(one_liker_list)
		else:
			likes_list = []
		one_music_list= [music_id,music_name, music_style,music_description,music_image,autor_name,autor_image, likes_list  ]
		all_musics_complet_list.append(one_music_list)
	all_musics_complet_list = sorted(all_musics_complet_list, key=operator.itemgetter(1))

	# serial
	serial_id_list = models.run_query_get_all("select culture_id from culture where type='serial';")
	all_serials_complet_list = []
	# one serial in list:        0        1          2           3               4         5            6           7
	#		one_serial_list= [serial_id,serial_name, serial_style,serial_description,serial_image,autor_name,autor_image, likes_list  ]
	for serial_id in serial_id_list:
		serial_name = models.run_query("select name from culture where culture_id='" + str(serial_id) + "';")
		serial_style= models.run_query("select style from culture where culture_id='" + str(serial_id) + "';")
		serial_description=models.run_query("select description from culture where culture_id='" + str(serial_id) + "';")
		autor_id = models.run_query("select author from culture where culture_id='" + str(serial_id) + "';")
		autor_name= models.run_query("select name from users where user_id='" + str(autor_id) + "';") 
		autor_image=models.run_query("select image from users where user_id='" + str(autor_id) + "';") 
		serial_image = models.run_query("select image from culture where culture_id='" + str(serial_id) + "';")
		if serial_image == None:
			serial_image="no_image.png"
		likes = models.run_query("select likes from culture where culture_id='" + str(serial_id) + "';")
		if likes != None:
			likes = json.loads(likes)
			likes_list = []
			for like_id in likes:
				liker_name= models.run_query("select name from users where user_id='" + str(like_id) + "';") 
				liker_image=models.run_query("select image from users where user_id='" + str(like_id) + "';") 	
				one_liker_list=[liker_name, liker_image]
				likes_list.append(one_liker_list)
		else:
			likes_list = []
		one_serial_list= [serial_id,serial_name, serial_style,serial_description,serial_image,autor_name,autor_image, likes_list  ]
		all_serials_complet_list.append(one_serial_list)
	all_serials_complet_list = sorted(all_serials_complet_list, key=operator.itemgetter(1))

	# book
	book_id_list = models.run_query_get_all("select culture_id from culture where type='book';")
	all_books_complet_list = []
	# one book in list:        0        1          2           3               4         5            6           7
	#		one_book_list= [book_id,book_name, book_style,book_description,book_image,autor_name,autor_image, likes_list  ]
	for book_id in book_id_list:
		book_name = models.run_query("select name from culture where culture_id='" + str(book_id) + "';")
		book_style= models.run_query("select style from culture where culture_id='" + str(book_id) + "';")
		book_description=models.run_query("select description from culture where culture_id='" + str(book_id) + "';")
		autor_id = models.run_query("select author from culture where culture_id='" + str(book_id) + "';")
		autor_name= models.run_query("select name from users where user_id='" + str(autor_id) + "';") 
		autor_image=models.run_query("select image from users where user_id='" + str(autor_id) + "';") 
		book_image = models.run_query("select image from culture where culture_id='" + str(book_id) + "';")
		if book_image == None:
			book_image="no_image.png"
		likes = models.run_query("select likes from culture where culture_id='" + str(book_id) + "';")
		if likes != None:
			likes = json.loads(likes)
			likes_list = []
			for like_id in likes:
				liker_name= models.run_query("select name from users where user_id='" + str(like_id) + "';") 
				liker_image=models.run_query("select image from users where user_id='" + str(like_id) + "';") 	
				one_liker_list=[liker_name, liker_image]
				likes_list.append(one_liker_list)
		else:
			likes_list = []
		one_book_list= [book_id,book_name, book_style,book_description,book_image,autor_name,autor_image, likes_list  ]
		all_books_complet_list.append(one_book_list)
	all_books_complet_list = sorted(all_books_complet_list, key=operator.itemgetter(1))

	# other
	other_id_list = models.run_query_get_all("select culture_id from culture where type='other';")
	all_others_complet_list = []
	# one other in list:        0        1          2           3               4         5            6           7
	#		one_other_list= [other_id,other_name, other_style,other_description,other_image,autor_name,autor_image, likes_list  ]
	for other_id in other_id_list:
		other_name = models.run_query("select name from culture where culture_id='" + str(other_id) + "';")
		other_style= models.run_query("select style from culture where culture_id='" + str(other_id) + "';")
		other_description=models.run_query("select description from culture where culture_id='" + str(other_id) + "';")
		autor_id = models.run_query("select author from culture where culture_id='" + str(other_id) + "';")
		autor_name= models.run_query("select name from users where user_id='" + str(autor_id) + "';") 
		autor_image=models.run_query("select image from users where user_id='" + str(autor_id) + "';") 
		other_image = models.run_query("select image from culture where culture_id='" + str(other_id) + "';")
		if other_image == None:
			other_image="no_image.png"
		likes = models.run_query("select likes from culture where culture_id='" + str(other_id) + "';")
		if likes != None:
			likes = json.loads(likes)
			likes_list = []
			for like_id in likes:
				liker_name= models.run_query("select name from users where user_id='" + str(like_id) + "';") 
				liker_image=models.run_query("select image from users where user_id='" + str(like_id) + "';") 	
				one_liker_list=[liker_name, liker_image]
				likes_list.append(one_liker_list)
		else:
			likes_list = []
		one_other_list= [other_id,other_name, other_style,other_description,other_image,autor_name,autor_image, likes_list  ]
		all_others_complet_list.append(one_other_list)
	all_others_complet_list = sorted(all_others_complet_list, key=operator.itemgetter(1))

	if liked == True:
		liked_value="yes"
		liked_value2=str(liked_val)
	else:
		liked_value="no"
		liked_value2=str(liked_val)

	response = render(request, "main/culture_main_template.html", dict(home_url=home_url, user_id=user_id, muj_list=list_of_passwords,liked_value=liked_value,liked_value2=liked_value2, image_user=image_user, user_name=user_name, all_films_complet_list=all_films_complet_list, all_musics_complet_list=all_musics_complet_list, all_serials_complet_list=all_serials_complet_list, all_books_complet_list=all_books_complet_list, all_others_complet_list=all_others_complet_list))
	return response




def new(request):
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")
	image_user = models.run_query("select image from users where email='" + useremail_cookie +  "'")
	user_name = models.run_query("select name from users where email='" + useremail_cookie +  "'")
	user_id =models.run_query("select user_id from users where email='" + useremail_cookie +  "'") 
	dict_items = models.run_query_get_all("select name from book;")
	dict_items.sort()
	list_of_passwords=[]
	for dict_item in dict_items:
		password_ids = models.run_query("select book_id from book where name='" + dict_item +  "'")
		list_of_passwords.append([dict_item, password_ids])


	if request.method == "POST":
		new_name = (request.POST["password_name"])
		new_name = (new_name[0]).capitalize() + new_name[1:]
		new_description = (request.POST["password_content"])
		try:
			myfile = request.FILES['myfile']
			fs = FileSystemStorage()
			new_img_id = models.get_new_image_id()
			new_image_name = new_img_id + ".png"
			filename = fs.save(new_image_name, myfile)
			uploaded_file_url = fs.url(filename)
			uploaded_img = True
		except:
			uploaded_img = False


		if new_name != "" and new_description != "":
			new_book_id = models.get_new_book_id()
			if uploaded_img == True:
				models.update_query("insert into book (book_id,name,description,image,author,timestamp) values (" + new_book_id +", '" + new_name + "', '" + new_description + "' , '" + new_image_name + "' , '" + str(user_id) + "' , '" + str(datetime.now()) +"') ")
			if uploaded_img == False:
				models.update_query("insert into book (book_id,name,description,image,author,timestamp) values (" + new_book_id +", '" + new_name + "', '" + new_description + "' , null , '" + str(user_id) + "' , '" + str(datetime.now()) +"') ")
	
			dict_items = models.run_query_get_all("select name from book;")
			dict_items.sort()
			list_of_passwords=[]
			for dict_item in dict_items:
				password_ids = models.run_query("select book_id from book where name='" + dict_item +  "'")
				list_of_passwords.append([dict_item, password_ids])
	
	
			status_message=" ULOŽENO "
			response = render(request, "main/new_template.html", dict(home_url=home_url, user_id=user_id, status_message=status_message,muj_list=list_of_passwords, user_name=user_name,image_user=image_user ))
			return response
		else:
			status_message=""
			error_msg = "Je třeba vyplnit název i popis."
			response = render(request, "main/new_template.html", dict(home_url=home_url,user_id=user_id, error_msg=error_msg, status_message=status_message,muj_list=list_of_passwords, user_name=user_name,image_user=image_user ))
			return response			

	else:
		status_message=""
		response = render(request, "main/new_template.html", dict(home_url=home_url, user_id=user_id, status_message=status_message,muj_list=list_of_passwords, user_name=user_name,image_user=image_user ))
		return response

def user(request, user_id):
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")
	image_user = models.run_query("select image from users where email='" + useremail_cookie +  "'")
	user_name = models.run_query("select name from users where email='" + useremail_cookie +  "'")
	user_id =models.run_query("select user_id from users where email='" + useremail_cookie +  "'") 	

	old_user_name = models.run_query("select name from users where email='" + useremail_cookie +  "'")
	old_user_password = models.run_query("select password from users where email='" + useremail_cookie +  "'")
	old_user_password = models.decode_password(old_user_password)
	old_user_salutation = models.run_query("select salutation from users where email='" + useremail_cookie +  "'")

	if request.method == "POST":
		if request.POST.get("save_user"):
			new_user_name = (request.POST["user_name_field"])
			new_user_password = (request.POST["user_password_field"])		
			new_user_salutation = (request.POST["user_salutation_field"])
			if new_user_name == "" or new_user_password == "" or new_user_salutation =="":
				error_msg = "Všechna pole musí být vyplněna"			
				status_message=""
				response = render(request, "main/user_template.html", dict(home_url=home_url, error_msg=error_msg, old_user_salutation=old_user_salutation, old_user_password=old_user_password, old_user_name=old_user_name, user_id=user_id,image_user=image_user,user_name=user_name,status_message=status_message))
				return response
			models.update_query("update users set name ='" + new_user_name + "' where user_id='" + str(user_id) + "';" )
			models.update_query("update users set salutation ='" + new_user_salutation + "' where user_id='" + str(user_id) + "';" )
			new_hash_password = models.encode_password(new_user_password)
			models.update_query("update users set password ='" + new_hash_password + "' where user_id='" + str(user_id) + "';" )
			status_message="Změny byly uloženy"
			error_msg=""
			response = render(request, "main/user_template.html", dict(home_url=home_url, error_msg=error_msg, old_user_salutation=new_user_salutation, old_user_password=new_user_password, old_user_name=new_user_name, user_id=user_id,image_user=image_user,user_name=user_name,status_message=status_message))
			response.set_cookie("hrstka_password", new_hash_password, max_age=None)
			return response
	else:
		status_message=""
		response = render(request, "main/user_template.html", dict(home_url=home_url, old_user_salutation=old_user_salutation, old_user_password=old_user_password, old_user_name=old_user_name, user_id=user_id,image_user=image_user,user_name=user_name,status_message=status_message))
		return response

def edit(request, password_id):
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")
	image_user = models.run_query("select image from users where email='" + useremail_cookie +  "'")
	user_name = models.run_query("select name from users where email='" + useremail_cookie +  "'")
	user_id =models.run_query("select user_id from users where email='" + useremail_cookie +  "'") 
	dict_items = models.run_query_get_all("select name from book;")
	dict_items.sort()
	list_of_passwords=[]
	for dict_item in dict_items:
		password_ids = models.run_query("select book_id from book where name='" + dict_item +  "'")
		list_of_passwords.append([dict_item, password_ids])

	actual_name = models.run_query("select name from book where book_id='" + password_id +  "'")
	actual_content = models.run_query("select description from book where book_id='" + password_id +  "'")
	image_name = models.run_query("select image from book where book_id='" + password_id +  "'")
	if image_name == None:
		no_detail_image = True
	else:
		no_detail_image = False

	if request.method == "POST":
		if request.POST.get("save"):
			new_name = (request.POST["password_name"])
			new_name = (new_name[0]).capitalize() + new_name[1:]
			new_description = (request.POST["password_content"])
			if new_name != "" and new_description != "":
				status_message = "Úspěšně uloženo"
				models.update_query("update book set name = '" + new_name + "' where book_id=" + password_id +  ";")
				models.update_query("update book set description = '" + new_description + "' where book_id=" + password_id +  ";") 
				models.update_query("update book set timestamp = '" + str(datetime.now()) + "' where book_id=" + password_id +  ";") 
				models.update_query("update book set author = '" + str(user_id)  + "' where book_id=" + password_id +  ";") 
				response = render(request, "main/edit_template.html", dict(home_url=home_url, user_id=user_id,no_detail_image=no_detail_image,password_image= image_name, status_message=status_message, actual_content=new_description, actual_name=new_name, muj_list=list_of_passwords, image_user=image_user, user_name=user_name ))
				return response
			else:
				status_message = ""
				error_msg = "Je potřeba vložit název i popis."
				response = render(request, "main/edit_template.html", dict(home_url=home_url, user_id=user_id,error_msg=error_msg,no_detail_image=no_detail_image, password_image=image_name,status_message=status_message, actual_content=actual_content,actual_name=actual_name, muj_list=list_of_passwords, image_user=image_user, user_name=user_name ))
				return response

		if request.POST.get("delete_word"):
			image_to_delete = models.run_query("select image from book where book_id='" + password_id +  "'")
			if image_to_delete != None:
				models.delete_view_img(image_to_delete)
			models.update_query("delete from book where book_id=" + password_id +  ";")
			status_message="Záznam smazán"
			dict_items = models.run_query_get_all("select name from book;")
			dict_items.sort()
			list_of_passwords=[]
			for dict_item in dict_items:
				password_ids = models.run_query("select book_id from book where name='" + dict_item +  "'")
				list_of_passwords.append([dict_item, password_ids])			
			img_deleted_message="Záznam smazán"
			response = render(request, "main/main_template.html", dict(home_url=home_url, user_id=user_id,img_deleted_message=img_deleted_message,muj_list=list_of_passwords, image_user=image_user, user_name=user_name))
			return response

		if request.POST.get("delete"):
			new_name = (request.POST["password_name"])
			new_description = (request.POST["password_content"])
			status_message=""
			img_deleted_message="Obrázek smazán"
			old_img_id = models.run_query("select image from book where book_id='" + password_id +  "'")
			models.delete_view_img(old_img_id)
			models.update_query("update book set image = null where book_id=" + password_id +  ";") 
			image_name = models.run_query("select image from book where book_id='" + password_id +  "'")
			models.update_query("update book set timestamp = '" + str(datetime.now()) + "' where book_id=" + password_id +  ";") 
			models.update_query("update book set author = '" + str(user_id)  + "' where book_id=" + password_id +  ";") 			
			if image_name == None:
				no_detail_image = True
			else:
				no_detail_image = False
			response = render(request, "main/edit_template.html", dict(home_url=home_url,  user_id=user_id,no_detail_image=no_detail_image, password_image=image_name,status_message=status_message, actual_content=actual_content,actual_name=actual_name, muj_list=list_of_passwords, image_user=image_user, user_name=user_name ))
			return response
		if  request.FILES['myfile']:
			myfile = request.FILES['myfile']
			fs = FileSystemStorage()
			new_img_id = models.get_new_image_id()
			new_image_name = new_img_id + ".png"
			filename = fs.save(new_image_name, myfile)
			uploaded_file_url = fs.url(filename)
			models.update_query("update book set timestamp = '" + str(datetime.now()) + "' where book_id=" + password_id +  ";") 
			models.update_query("update book set author = '" + str(user_id)  + "' where book_id=" + password_id +  ";") 
			models.update_query("update book set image = '" + new_image_name  + "' where book_id=" + password_id +  ";") 
			img_deleted_message="Obrázek nahrán"
			actual_name = models.run_query("select name from book where book_id='" + password_id +  "'")
			actual_content = models.run_query("select description from book where book_id='" + password_id +  "'")
			status_message=""
			image_name = models.run_query("select image from book where book_id='" + password_id +  "'")
			if image_name == None:
				no_detail_image = True
			else:
				no_detail_image = False
			response = render(request, "main/edit_template.html", dict(home_url=home_url, user_id=user_id,img_deleted_message=img_deleted_message, no_detail_image=no_detail_image,password_image= image_name, status_message=status_message, actual_content=actual_content, actual_name=actual_name, muj_list=list_of_passwords, image_user=image_user, user_name=user_name ))
			return response
	else:
		status_message = ""
		cosi = ""
		response = render(request, "main/edit_template.html", dict(home_url=home_url, user_id=user_id,no_detail_image=no_detail_image, password_image=image_name,status_message=status_message, actual_content=actual_content,actual_name=actual_name, muj_list=list_of_passwords, image_user=image_user, user_name=user_name ))
		return response

def view(request, password_id):
	# check if user is logged
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")


	if password_cookie_hash != None and useremail_cookie != None:
		db_password = models.run_query("select password from users where email='" + useremail_cookie +  "'")
		if password_cookie_hash == db_password:
			print("logged")
			logged_user = True
		else:
			logged_user=False
			print("unloged - bad password")
	else:
		logged_user=False
		print("unloged")

	if logged_user == False:
		return redirect('/login')


	dict_items = models.run_query_get_all("select name from book;")
	dict_items.sort()
	list_of_passwords=[]
	for dict_item in dict_items:
		password_ids = models.run_query("select book_id from book where name='" + dict_item +  "'")
		list_of_passwords.append([dict_item, password_ids])

	password_id = str(password_id)
	password_name = models.run_query("select name from book where book_id='" + password_id + "';")
	password_description = models.run_query("select description from book where book_id='" + password_id + "';")
	password_image = models.run_query("select image from book where book_id='" + password_id + "';")
	password_timestamp = models.run_query("select timestamp from book where book_id='" + password_id + "';")
	password_timestamp = password_timestamp.strftime("%d.%m. %Y, %H:%M:%S")
	image_user = models.run_query("select image from users where email='" + useremail_cookie +  "'")
	user_name = models.run_query("select name from users where email='" + useremail_cookie +  "'")
	user_id =models.run_query("select user_id from users where email='" + useremail_cookie +  "'") 
	image_name = models.run_query("select image from book where book_id='" + password_id +  "'")
	if image_name == None:
		no_detail_image = True
	else:
		no_detail_image = False
	password_author_id = models.run_query("select author from book where book_id='" + password_id + "';")
	password_author_name = models.run_query("select name from users where user_id='" + password_author_id + "';")
	password_author_image = models.run_query("select image from users where user_id='" + password_author_id + "';")
	response = render(request, "main/main_view_template.html", dict(home_url=home_url, user_id=user_id,password_id=password_id, password_author_image=password_author_image,no_detail_image=no_detail_image, user_name=user_name,muj_list=list_of_passwords,image_user=image_user, password_name=password_name, password_description=password_description, password_image=password_image, password_author_name=password_author_name, password_timestamp=password_timestamp ))

	return response

def index(request):

	# check if user is logged
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")
	if password_cookie_hash != None and useremail_cookie != None:
		db_password = models.run_query("select password from users where email='" + useremail_cookie +  "'")
		if password_cookie_hash == db_password:
			print("logged")
			logged_user = True
		else:
			logged_user=False
			print("unloged - bad password")
	else:
		logged_user=False
		print("unloged")

	if logged_user == False:
		return redirect('/login')




	dict_items = models.run_query_get_all("select name from book;")
	dict_items.sort()
	list_of_passwords=[]
	for dict_item in dict_items:
		password_id = models.run_query("select book_id from book where name='" + dict_item +  "'")
		list_of_passwords.append([dict_item, password_id])
	user_name = models.run_query("select name from users where email='" + useremail_cookie +  "'")
	image_user = models.run_query("select image from users where email='" + useremail_cookie +  "'")
	user_id =models.run_query("select user_id from users where email='" + useremail_cookie +  "'") 
	response = render(request, "main/main_template.html", dict(home_url=home_url, user_id=user_id, muj_list=list_of_passwords, image_user=image_user, user_name=user_name))
	if logged_user == False:
		response.delete_cookie("hrstka_password")
		response.delete_cookie("hrstka_email")
	return response


def login(request):
	# check if user is logged
	password_cookie_hash = request.COOKIES.get("hrstka_password")
	useremail_cookie = request.COOKIES.get("hrstka_email")
	if password_cookie_hash != None and useremail_cookie != None:
		db_password = models.run_query("select password from users where email='" + useremail_cookie +  "'")
		if password_cookie_hash == db_password:
			print("logged")
			logged_user = True
		else:
			logged_user=False
			print("unloged - bad password")
	else:
		logged_user=False
		print("unloged")

	if logged_user == True:
		error_msg=""
		salutation = models.run_query("select salutation from users where email='" + useremail_cookie +  "'")
		image_user = models.run_query("select image from users where email='" + useremail_cookie +  "'")
		response = render(request, "login/sucess_login_template.html", dict(home_url=home_url, salutation=salutation, image_user=image_user))
		return (response)

	if request.method == "POST":
		useremail_input = (request.POST["user_email"])
		password_input = (request.POST["user_password"])
		error_msg = ""
		hashed_user_password = models.encode_password(password_input)
		if hashed_user_password != None and useremail_input != None:
			db_password = models.run_query("select password from users where email='" + useremail_input +  "'")
			if hashed_user_password == db_password:
				error_msg=""
				salutation = models.run_query("select salutation from users where email='" + useremail_input +  "'")
				image_user = models.run_query("select image from users where email='" + useremail_input +  "'")

				response = render(request, "login/sucess_login_template.html", dict(home_url=home_url, salutation=salutation, image_user=image_user))
				response.set_cookie("hrstka_password", hashed_user_password, max_age=None)
				response.set_cookie("hrstka_email", useremail_input, max_age=None)
				return (response)
			else:
				error_msg = {'error_msg' : " neplatné heslo nebo email", 'error_msg2': "neplatné heslo" }
				response = render(request, "login/login_template.html", dict(home_url=home_url,error_msg =error_msg))
				return (response)
	else:
		error_msg = ""
		response = render(request, "login/login_template.html", dict(home_url=home_url, error_msg=error_msg))
		return (response)