function show_menu(){
	var word_list = document.getElementById("main_frame_menu_wordlist");
	var show_button = document.getElementById("main_frame_menu_menubutton_show_div");
	word_list.style.display = "block";
	show_button.style.display = "none";
} 
function hide_menu(){
	var word_list = document.getElementById("main_frame_menu_wordlist");
	var show_button = document.getElementById("main_frame_menu_menubutton_show_div");
	word_list.style.display = "none";
	show_button.style.display = "block";
} 
function show_film(){
	var show_button = document.getElementById("all_lines_culture_container");
	show_button.style.display = "block";
	var show_button2 = document.getElementById("show_film_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("hide_film_arrow");
	show_button2.style.display = "block";
} 

function hide_film(){
	var show_button = document.getElementById("all_lines_culture_container");
	show_button.style.display = "none";
	var show_button2 = document.getElementById("hide_film_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("show_film_arrow");
	show_button2.style.display = "block";
} 

function show_music(){
	var show_button = document.getElementById("all_lines_culture_container_music");
	show_button.style.display = "block";
	var show_button2 = document.getElementById("show_music_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("hide_music_arrow");
	show_button2.style.display = "block";
} 

function hide_music(){
	var show_button = document.getElementById("all_lines_culture_container_music");
	show_button.style.display = "none";
	var show_button2 = document.getElementById("hide_music_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("show_music_arrow");
	show_button2.style.display = "block";
} 


function hide_serial(){
	var show_button = document.getElementById("all_lines_culture_container_serial");
	show_button.style.display = "none";
	var show_button2 = document.getElementById("hide_serial_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("show_serial_arrow");
	show_button2.style.display = "block";
} 
function show_serial(){
	var show_button = document.getElementById("all_lines_culture_container_serial");
	show_button.style.display = "block";
	var show_button2 = document.getElementById("show_serial_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("hide_serial_arrow");
	show_button2.style.display = "block";
} 


function hide_book(){
	var show_button = document.getElementById("all_lines_culture_container_book");
	show_button.style.display = "none";
	var show_button2 = document.getElementById("hide_book_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("show_book_arrow");
	show_button2.style.display = "block";
} 
function show_book(){
	var show_button = document.getElementById("all_lines_culture_container_book");
	show_button.style.display = "block";
	var show_button2 = document.getElementById("show_book_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("hide_book_arrow");
	show_button2.style.display = "block";
} 

function hide_empty_img(){
var images = document.getElementsByTagName('img'); 
for(var i = 0; i < images.length; i++) {
	console.log("IMG");
  var image_src=images[i].getAttribute("src");
  
  result = image_src.includes("no_image.png");
  if (result == true) {images[i].style.display = "none";}
}
} 
 





function hide_other(){
	var show_button = document.getElementById("all_lines_culture_container_other");
	show_button.style.display = "none";
	var show_button2 = document.getElementById("hide_other_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("show_other_arrow");
	show_button2.style.display = "block";
} 
function show_other(){
	var show_button = document.getElementById("all_lines_culture_container_other");
	show_button.style.display = "block";
	var show_button2 = document.getElementById("show_other_arrow");
	show_button2.style.display = "none";
	var show_button2 = document.getElementById("hide_other_arrow");
	show_button2.style.display = "block";
} 

function view_scroll(){
	hide_empty_img()
	var status_value_element = document.getElementById("liked_status");
	var status_value = status_value_element.getAttribute("value");
	var culture_id = status_value_element.getAttribute("value2");

	if (status_value == "yes") {
 		console.log('cultureID_'+culture_id)
 		show_film();
 		show_music();
 		show_serial();
 		show_book();
 		show_other();

 		document.getElementById('cultureID_'+culture_id).scrollIntoView(true);


}
} 
