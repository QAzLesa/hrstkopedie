from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('new/', views.new, name='new'),
    path('culture/', views.culture, name='culture'),
    path('newculture/', views.newculture, name='newculture'),

	url(r'^like/(?P<like_id>\d+)/', views.like, name='like'),
	url(r'^view/(?P<password_id>\d+)/', views.view, name='view'),
	url(r'^edit/(?P<password_id>\d+)/', views.edit, name='edit'),
	url(r'^user/(?P<user_id>\d+)/', views.user, name='user')

   ]

