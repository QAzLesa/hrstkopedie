from django.db import models
from hrstkopedie.settings import *
import base64
import psycopg2
import subprocess

def encode_password(password):
	hash = (base64.b64encode(password.encode("utf-8")))
	hash = hash.decode("utf-8")
	return hash

def get_new_image_id():
	response = subprocess.check_output("ls -1t brain/static/images/subjects/", shell=True)
	response = response.decode("utf-8")
	response = response.splitlines()
	print("--------------")
	for i in response:
		print("--" + i +"--")
	print("--------------")

	try:
		response.remove("no_image.png")
	except:
		pass
	list_int_id = []
	for i in response:
		list_int_id.append(int(i.replace(".png","")))
	list_int_id = sorted(list_int_id)
	try:
		last_id = list_int_id[-1]
	except:
		last_id = "0"
	print("LAST ID FOUND: "+ str(last_id))
	new_id = str(last_id + 1)
	print("NEW ID: "+ str(new_id))

	return new_id


def get_new_book_id():
	old_id = run_query("select book_id from book order by book_id desc limit 1;")
	new_id = str(old_id + 1)
	return new_id

def get_new_culture_id():
	old_id = run_query("select culture_id from culture order by culture_id desc limit 1;")
	new_id = str(old_id + 1)
	return new_id

def delete_view_img(name):
	response = subprocess.check_output("rm brain/static/images/subjects/" + name, shell=True)

def decode_password(hash):
	password = (base64.b64decode(hash).decode("utf-8"))
	return password

def connect_db():
	database_dict = DATABASES['default']
	dbname = database_dict['NAME']
	dbuser = database_dict['USER']
	dbpassword = database_dict['HOST']
	dbport = database_dict['PORT']
	connection_data = "dbname='" + dbname + "' user='" + dbuser +"' password='" + dbpassword + "'"
	try:
		session = psycopg2.connect(connection_data)
	except Exception as err:
		print("I am unable to connect database: " + str(err))
	postgres_cursor = session.cursor()
	return postgres_cursor, session

def run_query(query):
	postgres_cursor, session = connect_db()
	postgres_cursor.execute(query)
	try:
		results = postgres_cursor.fetchone()[0]
		return results
	except:
		return False

def update_query(query):
	cursor, conn = connect_db()
	cursor.execute(query)
	conn.commit()
	cursor.close()

def run_query_get_all(query):
	postgres_cursor, session = connect_db()
	postgres_cursor.execute(query)
	try:
		results = postgres_cursor.fetchall()
		results_output = []
		for i in results:
			results_output.append(i[0])

		return results_output
	except:
		return False
